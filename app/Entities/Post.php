<?php

namespace App\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Post
 * @property Carbon|null $published_at
 * @property string $title
 * @property string $content
 */
class Post extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'content',
        'featured_image',
        'is_featured',
        'published_at',
        'user_id'
    ];

    protected $dates = [
        'published_at',
        'deleted_at'
    ];
    
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($post) {
            if (!$post->slug) {
                $post->slug = $post->makeSlug();
            }
        });
    }
    
    public function makeSlug()
    {
        $slug = str_slug($this->title);
        do {
            $exists = $this->where('slug', $slug)->exists();
            if ($exists) {
                $slug .= '-' . str_random(4);
            }
        } while ($exists);
        
        return $slug;
    }
    
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @param Carbon|null $at
     * @return $this
     */
    public function publish(Carbon $at = null)
    {
        $this->published_at = $at;
        $this->save();
        return $this;
    }

    public function feature()
    {

    }

    /**
     * @return string
     */
    public function getUrlAttribute()
    {
        return route('posts.show', ['post-slug' => $this->slug]);
    }
}
