<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Entities\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\App\Entities\Post::class, function (\Faker\Generator $faker) {
    return [
        'title' => $title = $faker->unique()->realText(20),
        'slug' => str_slug($title),
        'content' => $faker->realText(),
        'user_id' => function () {
            return factory(App\Entities\User::class)->create()->id;
        }
    ];
});

$factory->define(\App\Entities\Comment::class, function (\Faker\Generator $faker) {
    return [
        'content' => $faker->realText(100),
        'post_id' => function () {
            return factory(App\Entities\Post::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\Entities\User::class)->create()->id;
        }
    ];
});