<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Entities\User::class)
            ->times(10)->create()
            ->each(function ($user) {

                factory(\App\Entities\Post::class)
                    ->times(5)
                    ->create(['user_id' => $user->id])
                    ->each(function ($post) {
                        factory(\App\Entities\Comment::class)
                            ->times(5)
                            ->create([
                                'post_id' => $post->id
                            ]);
                    });
            });
//        factory(\App\Entities\Comment::class)
//            ->times(15)
//            ->create();
    }
}
