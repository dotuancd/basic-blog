@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($post->exists)
            <h3>Update post: {{$post->title}}</h3>
        @else
            <h3>New post</h3>
        @endif
        <form action="{{route('posts.store')}}" method="post" class="form-horizontal">
            {{csrf_field()}}
            <div class="form-group">
                <label class="control-label col-md-3" for="title">Title</label>
                <div class="col-md-9">
                    <input type="text" value="{{old('title', $post->title)}}" class="form-control" name="title">
                    @if ($errors->has('title'))
                        <div class="alert alert-danger">
                            {{$errors->first('title')}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="content" class="control-label col-md-3">Content</label>
                <div class="col-md-9">
                    <textarea rows="4" type="text" class="form-control" name="content">{{old('content', $post->content)}}</textarea>
                    @if ($errors->has('content'))
                        <div class="alert alert-danger">
                            {{$errors->first('content')}}
                        </div>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-success">
                        <span class="glyphicon glyphicon-floppy-disk"></span> Save
                    </button>
                    <a href="{{route('posts.index')}}" class="btn btn-link">Back</a>
                </div>
            </div>
        </form>
    </div>
@endsection