@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="blog-post-navigation row">
                    <div class="pull-right">
                        <a href="{{route('posts.create')}}" class="btn btn-success">
                            <span class="glyphicon glyphicon-plus-sign"></span> Post
                        </a>
                    </div>
                </div>
                @foreach($posts as $post)
                    @include('posts.post-loop')
                @endforeach
            </div>
        </div>
    </div>
@endsection