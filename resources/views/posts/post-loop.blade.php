<div class="blog-post">
    <div class="blog-post-title">
        <a href="{{$post->url}}">
            {{$post->title}}
        </a>
    </div>
    <p class="blog-post-meta">
        {{$post->created_at->toFormattedDateString()}} by
        <a href="{{$post->author->url}}">
            {{$post->author->name}}
        </a>
    </p>
    <p>
        {{str_limit($post->content, 100)}}
    </p>
    <hr>
</div>