@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <h2>{{$post->title}}</h2>
            <p>
                {{$post->content}}
            </p>
            <hr />
            <h3>Author</h3>
            <p>
                <a href="{{$post->author->url}}">{{$post->author->name}}</a>
            </p>
            <hr>
            <h3>Comments:</h3>
            @foreach($post->comments as $comment)
                <div>
                    <a href="{{$comment->user->url}}">{{$comment->user->name}}</a>
                    says {{$comment->content}}
                </div>
            @endforeach
        </div>
    </div>
@endsection