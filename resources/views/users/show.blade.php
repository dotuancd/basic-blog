@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8">
            <h2>{{$user->name}}</h2>
            <hr />
            <h3>Posts</h3>
            @foreach($user->posts as $post)
                <div>
                    <a href="{{$post->url}}">{{$post->title}}</a>
                </div>
            @endforeach
        </div>
    </div>
@endsection