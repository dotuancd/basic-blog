<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::resource('posts', 'PostController', ['except' => ['destroy', 'show']]);
Route::get('/{post_slug}.html', 'PostController@show')->name('posts.show');
Route::get('/{post_slug}.html/delete', 'PostController@destroy')->name('posts.destroy');
Route::get('authors/{author}', 'UserController@show')->name('users.show');
